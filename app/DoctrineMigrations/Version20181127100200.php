<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181127100200 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE attachments (id INT AUTO_INCREMENT NOT NULL, attachment_url VARCHAR(250) NOT NULL, created DATETIME NOT NULL, modified DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attachment_storage (attachment_id INT NOT NULL, storage_id INT NOT NULL, INDEX IDX_9CEF4EB2464E68B (attachment_id), INDEX IDX_9CEF4EB25CC5DB90 (storage_id), PRIMARY KEY(attachment_id, storage_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE storage_informations (id INT AUTO_INCREMENT NOT NULL, storage_id INT DEFAULT NULL, title VARCHAR(150) NOT NULL, description TEXT DEFAULT NULL, html_description TEXT DEFAULT NULL, created DATETIME NOT NULL, modified DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_23F5E42E5CC5DB90 (storage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attachment_storage ADD CONSTRAINT FK_9CEF4EB2464E68B FOREIGN KEY (attachment_id) REFERENCES attachments (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE attachment_storage ADD CONSTRAINT FK_9CEF4EB25CC5DB90 FOREIGN KEY (storage_id) REFERENCES storages (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE storage_informations ADD CONSTRAINT FK_23F5E42E5CC5DB90 FOREIGN KEY (storage_id) REFERENCES storages (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attachment_storage DROP FOREIGN KEY FK_9CEF4EB2464E68B');
        $this->addSql('DROP TABLE attachments');
        $this->addSql('DROP TABLE attachment_storage');
        $this->addSql('DROP TABLE storage_informations');
    }
}
