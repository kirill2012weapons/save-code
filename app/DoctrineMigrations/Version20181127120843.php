<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181127120843 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE attachment_storage');
        $this->addSql('ALTER TABLE attachments ADD storage_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE attachments ADD CONSTRAINT FK_47C4FAD65CC5DB90 FOREIGN KEY (storage_id) REFERENCES storages (id)');
        $this->addSql('CREATE INDEX IDX_47C4FAD65CC5DB90 ON attachments (storage_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE attachment_storage (attachment_id INT NOT NULL, storage_id INT NOT NULL, INDEX IDX_9CEF4EB2464E68B (attachment_id), INDEX IDX_9CEF4EB25CC5DB90 (storage_id), PRIMARY KEY(attachment_id, storage_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attachment_storage ADD CONSTRAINT FK_9CEF4EB2464E68B FOREIGN KEY (attachment_id) REFERENCES attachments (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE attachment_storage ADD CONSTRAINT FK_9CEF4EB25CC5DB90 FOREIGN KEY (storage_id) REFERENCES storages (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE attachments DROP FOREIGN KEY FK_47C4FAD65CC5DB90');
        $this->addSql('DROP INDEX IDX_47C4FAD65CC5DB90 ON attachments');
        $this->addSql('ALTER TABLE attachments DROP storage_id');
    }
}
