<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Type;
use AppBundle\Form\AddTypeEntity;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Service\FileAttachment;


class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/set-type-name", name="homepage_set_type_name")
     */
    public function indexSetTypeName(Request $request,
                                     EntityManagerInterface $entityManager) {

        $form = $this->createForm(AddTypeEntity::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $type = new Type();

            $type->setName($form->getData()['name']);

            $entityManager->persist($type);

            $entityManager->flush();

            return $this->render('default/set-type-name.html.twig', [
                'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
                'form' => $form->createView(),
                'submitted' => 'Form is Submitted',
            ]);
        }

        return $this->render('default/set-type-name.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
        ]);
    }

}
