<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Attachment;
use AppBundle\Entity\Storage;
use AppBundle\Entity\StorageInformation;
use AppBundle\Entity\Tag;
use AppBundle\Entity\Task;
use AppBundle\Form\Storage\Test\TaskType;
use AppBundle\Form\StorageAddActionType;
use AppBundle\Service\StorageGeneratorUrl;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Type;

/**
 * @Route(
 *     "/storage",
 *     name="storage_"
 * )
 */
class StorageController extends Controller {

    /**
     * @Route(
     *     "/",
     *     name="index_show_all"
     * )
     */
    public function indexAction(
        EntityManagerInterface $entityManager,
        StorageGeneratorUrl $storageGeneratorUrl
    ) {

        $storageRepository = $entityManager->getRepository(Storage::class);

        return $this->render(
            'storage/index_show_all.html.twig',
            [
                'storages' => $storageRepository->getAllStorages(),
                'storage_generator_url' => $storageGeneratorUrl,
            ]
        );

    }

    /**
     * @Route(
     *     "/rewrite",
     *     name="rewrite_show_all"
     * )
     */
    public function rewriteAction(
        EntityManagerInterface $entityManager,
        StorageGeneratorUrl $storageGeneratorUrl
    ) {

        $storageRepository = $entityManager->getRepository(Storage::class);

        return $this->render(
            'storage/index_rewrite.html.twig',
            [
                'storages' => $storageRepository->getAllStorages(),
                'storage_generator_url' => $storageGeneratorUrl,
            ]
        );

    }

    /**
     * @Route(
     *     "/new",
     *     name="new"
     * )
     */
    public function newStorageAction(
        Request $request,
        EntityManagerInterface $entityManager
    ) {

        $typeRepository = $entityManager
            ->getRepository(Type::class);

        $newStorage = new Storage();

        $newStorageInformation = new StorageInformation();

        $newStorage->setStorageInformation($newStorageInformation);

        if ( $request->get('storage_add_action') && isset($request->get('storage_add_action')['attachments'])) {
            foreach ($request->get('storage_add_action')['attachments'] as $key) {
                $newAttachment = new Attachment();
                $newStorage->setAttachments($newAttachment);
            }
        } else {
            $newAttachment = new Attachment();
            $newStorage->setAttachments($newAttachment);
        }
        
        $formCreatingStorage = $this->createForm(StorageAddActionType::class, $newStorage);

        $formCreatingStorage->handleRequest($request);

        if ($formCreatingStorage->isSubmitted()) {

            $type = $typeRepository
                ->find($formCreatingStorage->get('type_id')->getData());

            $newStorage->setType($type);

            if ($formCreatingStorage->isValid()) {

                if ($type) {

                    foreach ($formCreatingStorage->get('attachments')->all() as $key => $value) {
                        $file = $value->get('file')->getNormData();
                        $newStorage->getAttachments()[$key]->setAttachmentURL($file);
                    }

                    $entityManager->persist($newStorage);
                    $entityManager->flush();

                    return $this->redirectToRoute('storage_index_show_all');

                }

            }

            return $this->render(
                'storage/new-storage.html.twig',
                [
                    'types' => $typeRepository->getListType(),
                    'form_storage' => $formCreatingStorage->createView(),
                    'dump_storage' => $newStorage,
                    'type' => $type,
                ]
            );

        }

        return $this->render(
            'storage/new-storage.html.twig',
            [
                'types' => $typeRepository->getListType(),
                'form_storage' => $formCreatingStorage->createView(),
                '__DIR__' => __DIR__,
            ]
        );

    }

    /**
     * @Route(
     *     "/rewrite/delete/{id}",
     *     name="delete_by_id",
     *     requirements={"id"="\d+"}
     * )
     */
    public function deleteStorageAction(
        $id,
        Request $request,
        EntityManagerInterface $entityManager
    ) {

        $storageRepository = $entityManager->getRepository(Storage::class);
        $storage = $storageRepository->find($id);

        if ($storage->isExist()) {
            $entityManager->remove($storage);
            $entityManager->flush();
            return $this->redirectToRoute('storage_rewrite_show_all');
        }
        return $this->redirectToRoute('storage_rewrite_show_all');

    }

    /**
     * @Route(
     *     "/{id}",
     *     name="show_by_id",
     *     requirements={"id"="\d+"}
     * )
     */
    public function showStorageAction(
        $id
    ) {

    }

    /**
     * @Route(
     *     "/rewrite/modify/{id}",
     *     name="modify_by_id",
     *     requirements={"id"="\d+"}
     * )
     */
    public function modifyAction(
        $id,
        Request $request,
        EntityManagerInterface $entityManager
    ) {

        if (!$id) return $this->render('storage/rewrite_show_all.html.twig');

        $storageRepository = $entityManager
            ->getRepository(Storage::class)
            ->find($id);

        $typeRepository = $entityManager
            ->getRepository(Type::class)
            ->getListType();

        $formStorage = $this->createForm(StorageAddActionType::class, $storageRepository);

        $typeID = $storageRepository->getType()->getID();

        return $this->render(
            'storage/modify_by_id.html.twig',
            [
                'form_storage' => $formStorage->createView(),
                'types' => $typeRepository,
                'type_id_g' => $typeID,
            ]
        );

    }

    /**
     * @Route(
     *     "/test",
     *     name="test",
     * )
     */
    public function testAction(
        Request $request
    ) {

        $task = new Task();

        $tag1 = new Tag();
        $tag1->setName('tag1');
        $task->setTag($tag1);

        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->render(
                'storage/test.html.twig',
                [
                    'form' => $form->createView(),
                ]
            );
        }

        return $this->render(
            'storage/test.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

    }
}