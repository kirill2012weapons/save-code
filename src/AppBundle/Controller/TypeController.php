<?php

namespace AppBundle\Controller;

use AppBundle\Form\UpdateType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Type;
use AppBundle\Form\AddTypeEntity;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/types", name="types_")
 */
class TypeController extends Controller {

    /**
     * @Route("/", name="index_show_all")
     */
    public function indexAction(
        EntityManagerInterface $entityManager
    ) {

        $repositoryType = $entityManager->getRepository(Type::class);

        return $this->render('types/index.html.twig', [
            'types' => $repositoryType->getListType(),
        ]);

    }

    /**
     * @Route("/new", name="new")
     */
    public function newTypeAction(
        Request $request,
        EntityManagerInterface $entityManager) {


        $newType = new Type();
        $form = $this->createForm(AddTypeEntity::class, $newType);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($newType);
            $entityManager->flush();

            return $this->redirectToRoute('types_index_show_all');

        }

        return $this->render('types/new-type.html.twig', [
            'type_form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/delete/{id}", name="delete_by_id", requirements={"id"="\d+"})
     */
    public function deleteTypeAction(
        $id,
        Request $request,
        EntityManagerInterface $entityManager) {

        $typeEntity = $entityManager->getRepository(Type::class);
        $type = $typeEntity->find($id);

        if ($type->isExist()) {
            $entityManager->remove($type);
            $entityManager->flush();
            return $this->redirectToRoute('types_index_show_all');
        }
        return $this->redirectToRoute('types_index_show_all');

    }

    /**
     * @Route("/modify/{id}", name="modify_by_id", requirements={"id"="\d+"})
     */
    public function modifyTypeAction(
        $id,
        Request $request,
        EntityManagerInterface $entityManager) {

        $type = new Type();

        $typeManager = $entityManager
            ->getRepository(Type::class)
            ->find($id);

        if (!$typeManager) {
            return $this->redirectToRoute('types_index_show_all');
        }

        $form = $this->createForm(UpdateType::class, $type);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $typeManager->setName($request->get('update')['name']);
                $typeManager->setDescription($request->get('update')['description']);

                $entityManager->flush();

                return $this->redirectToRoute('types_index_show_all');

            } else return $this->render('types/modify-type.html.twig', [
                    'form' => $form->createView(),
                    'typeManager' => $typeManager,
                ]);
        }

        return $this->render('types/modify-type.html.twig', [
            'form' => $form->createView(),
            'typeManager' => $typeManager,
        ]);

    }


}