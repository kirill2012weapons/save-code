<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Service\FileAttachment;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="attachments")
 */
class Attachment {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(
     *     name="id",
     *     type="integer"
     * )
     */
    private $id;

    /**
     * @ORM\Column(
     *     name="attachment_url",
     *     type="string",
     *     length=250
     * )
     */
    private $attachment_url;

    /**
     * @ORM\Column(
     *     name="attachment_description",
     *     type="string",
     *     length=250
     * )
     * @Assert\NotBlank(message="Not BLANK")
     * @Assert\NotNull(message="Error men not null")
     * @Assert\Length(
     *     max=50,
     *     maxMessage="Max must bee lower then 50",
     *     min=2,
     *     minMessage="Must be more then 2"
     * )
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9\s]+$/",
     *     message="Only Eng Letters"
     * )
     */
    private $attachment_description;

    /**
     * @ORM\Column(
     *     name="attachment_origin_name",
     *     type="string",
     *     length=250
     * )
     */
    private $attachment_origin_name;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Storage",
     *     inversedBy="attachments"
     * )
     * @ORM\JoinColumn(
     *     name="storage_id",
     *     referencedColumnName="id"
     * )
     */
    private $storage;

    /**
     * @ORM\Column(
     *     name="created",
     *     type="datetime"
     * )
     */
    private $created;

    /**
     * @ORM\Column(
     *     name="modified",
     *     type="datetime",
     *     nullable=true
     * )
     */
    private $modified;

    public function getAttachmentOriginName() {
        return $this->attachment_origin_name;
    }

    public function setAttachmentOriginName($name) {
        $this->attachment_origin_name = $name;
    }

    public function getID() {
        return $this->id;
    }

    public function getAttachmentURL() {
        return $this->attachment_url;
    }

    public function getStorage() {
        return $this->storage;
    }

    public function setStorage(Storage $storage) {
        $this->storage = $storage;
    }

    public function getAttachmentDescription() {
        return $this->attachment_description;
    }

    public function setAttachmentDescription($attachmentDescription) {
        $this->attachment_description = $attachmentDescription;
    }

    public function setAttachmentURL(UploadedFile $uploadedFile) {

        $name = new FileAttachment();

        $this->attachment_url = $name->saveFile($uploadedFile);

    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedDate() {
        $this->created = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateModifiedDate() {
        $this->modified = new \DateTime();
    }


}
