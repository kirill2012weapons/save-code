<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StorageRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="storages")
 */
class Storage {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="storages")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * @Assert\Valid()
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="StorageInformation", mappedBy="storage", cascade={"persist"})
     * @Assert\Valid(traverse=true)
     */
    private $storageInformation;

    /**
     * @ORM\OneToMany(targetEntity="Attachment", mappedBy="storage", cascade={"persist"})
     * @Assert\Valid(traverse=true)
     */
    private $attachments;

    public function __construct() {
        $this->attachments = new ArrayCollection();
    }

    public function getID() {
        return $this->id;
    }

    public function getType() {
        return $this->type;
    }

    public function setType(Type $type) {
        $type->setStorages($this);
        $this->type = $type;
    }

    public function getStorageInformation() {
        return $this->storageInformation;
    }

    public function setStorageInformation(StorageInformation $storageInformation) {
        $storageInformation->setStorage($this);
        $this->storageInformation = $storageInformation;
    }

    public function getAttachments() {
        return $this->attachments;
    }

    public function setAttachments(Attachment $attachment) {
        $attachment->setStorage($this);
        $this->attachments->add($attachment);
    }

    public function getCreatedDate() {
        return $this->created->format('Y-m-d H:i:s');
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedDate() {
        $this->created = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateModifiedDate() {
        $this->modified = new \DateTime();
    }

}

