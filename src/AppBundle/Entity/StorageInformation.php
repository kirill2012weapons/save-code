<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="storage_informations")
 */
class StorageInformation {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(
     *     name="id",
     *     type="integer"
     * )
     */
    private $id;

    /**
     * @ORM\Column(
     *     name="title",
     *     type="string",
     *     length=250
     * )
     * @Assert\NotBlank(message="Not BLANK")
     * @Assert\NotNull(message="Error men not null")
     * @Assert\Length(
     *     max=50,
     *     maxMessage="Max must bee lower then 50",
     *     min=2,
     *     minMessage="Must be more then 2"
     * )
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9\s]+$/",
     *     message="Only Eng Letters"
     * )
     */
    private $title;

    /**
     * @ORM\Column(
     *     name="description",
     *     type="text",
     *     length=2000,
     *     nullable=true
     * )
     * @Assert\NotBlank(message="Not BLANK")
     * @Assert\NotNull(message="Error men not null")
     * @Assert\Length(
     *     max=2000,
     *     maxMessage="Max must bee lower then 50",
     *     min=10,
     *     minMessage="Must be more then 10"
     * )
     */
    private $description = null;

    /**
     * @ORM\Column(
     *     name="html_description",
     *     type="text",
     *     length=5000,
     *     nullable=true
     * )
     *
     */
    private $htmlDescription = null;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @ORM\OneToOne(targetEntity="Storage", inversedBy="storageInformation")
     * @ORM\JoinColumn(name="storage_id", referencedColumnName="id")
     */
    private $storage;

    public function getID() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getHtmlDescription() {
        return $this->htmlDescription;
    }

    public function getStorage() {
        return $this->storage;
    }
    
    public function setTitle($title) {
        $this->title = $title;
    }

    public function setStorage(Storage $storage) {
        $this->storage = $storage;
    }
    
    public function setDescription($description) {
        $this->description = $description;
    }

    public function setHtmlDescription($htmlDescription) {
        $this->htmlDescription = $htmlDescription;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedDate() {
        $this->created = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateModifiedDate() {
        $this->modified = new \DateTime();
    }

}