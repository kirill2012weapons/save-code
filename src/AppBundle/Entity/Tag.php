<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Tag
{

    /**
     * @Assert\NotNull(message="Error men not null")
     * @Assert\NotBlank(
     *     message="Title is not Blank!"
     * )
     * @Assert\Type(
     *     type="string",
     *     message="Must be string"
     * )
     * @Assert\Length(
     *     min=5,
     *     max=250
     * )
     */
    private $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

}