<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class Task {

    /**
     * @Assert\NotNull(message="Error men not null")
     * @Assert\NotBlank(
     *     message="Title is not Blank!"
     * )
     * @Assert\Type(
     *     type="string",
     *     message="Must be string"
     * )
     * @Assert\Length(
     *     min=5,
     *     max=250
     * )
     */
    protected $description;

    protected $tag;

    public function __construct() {
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getTags() {
        return $this->tag;
    }

    public function setTag($tag) {
        $this->tag = $tag;
    }
}