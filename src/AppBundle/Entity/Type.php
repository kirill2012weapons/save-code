<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Routing\Router;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TypeRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="types")
 */
class Type {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     * @Assert\NotBlank(message="Not BLANK")
     * @Assert\NotNull(message="Error men not null")
     * @Assert\Length(
     *     max=50,
     *     maxMessage="Max must bee lower then 50",
     *     min=2,
     *     minMessage="Must be more then 2"
     * )
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z]+$/",
     *     message="Only Eng Letters"
     * )
     */
    private $name;

    /**
     * @ORM\Column(name="description", type="string", length=150)
     * @Assert\NotBlank(message="Not BLANK")
     * @Assert\NotNull(message="Error men not null")
     * @Assert\Length(
     *     max=150,
     *     maxMessage="Max must bee lower then 150",
     *     min=10,
     *     minMessage="Must be more then 10"
     * )
     */
    private $description;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @ORM\OneToMany(targetEntity="Storage", mappedBy="type")
     */
    private $storages;

    public function __construct() {
        $this->storages = new ArrayCollection();
    }

    public function getStorages() {
        return $this->storages;
    }

    public function setStorages(Storage $storage) {
        $this->storages->add($storage);
    }

    public function getDescription() {
        return $this->description;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getID() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getCreated() {
        return $this->created->format('Y-m-d H:i:s');
    }

    public function getModified() {
        if (!is_null($this->modified)) return $this->modified->format('Y-m-d H:i:s');
        else return 'No modified';
    }

    public function isExist() {
        if (is_numeric($this->getID())) return true;
        return false;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedDate() {
        $this->created = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateModifiedDate() {
        $this->modified = new \DateTime();
    }

    public function __toString(){
        return (string)$this->id;
    }

}