<?php

namespace AppBundle\Form;

use AppBundle\Entity\Attachment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Asserts;

class AttachmentType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('attachment_url', TextType::class)
            ->add('attachment_description', TextType::class)
            ->add('file', FileType::class, [
                'mapped' => false,
                'constraints' => [
                    new Asserts\NotBlank(),
                    new Asserts\NotNull(),
                    new Asserts\File([
                        'maxSize' => '50M',
                    ]),
                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver
            ->setDefaults([
                'data_class' => Attachment::class
            ]);

    }

}