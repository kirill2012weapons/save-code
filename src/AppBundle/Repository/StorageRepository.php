<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Storage;
use Doctrine\ORM\EntityRepository;

class StorageRepository extends EntityRepository {

    public function getAllStorages() {

        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('s')
            ->from(Storage::class, 's')
            ->orderBy('s.created', 'DESC')
            ->getQuery()
            ->execute();

    }

}