<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Type;

class TypeRepository extends EntityRepository {

    public function getListType() {

        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('t')
            ->from(Type::class, 't')
            ->orderBy('t.created', 'DESC')
            ->getQuery()
            ->execute();

    }

}