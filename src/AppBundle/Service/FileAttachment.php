<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileAttachment {

    private $file;

    private $fileName = '';

    private $errors = null;

    private $web_dir = __DIR__ . '../../../../web/uploads';

    public function getUploadDir() {
        return $this->web_dir;
    }

    public function saveFile(UploadedFile $file) {

        $this->file = $file;

        $this->fileName = $this->generateUniqueFileName() . '.' . $this->file->getClientOriginalExtension();

        try {

            $this->file->move(
                $this->getUploadDir(),
                $this->fileName
            );
            
        } catch (FileException $e) {
            $this->errors = $e->getMessage();
            return false;
        }

        return $this->fileName;

    }

    public function getErrors() {
        if (!empty($this->errors)) return $this->errors;
        else return false;
    }

    private function generateUniqueFileName() {
        return md5(uniqid());
    }

}