<?php
namespace AppBundle\Service;

use AppBundle\Entity\Storage;
use Symfony\Component\Routing\RouterInterface;

class StorageGeneratorUrl {

    private $router;

    public function __construct(RouterInterface $router) {
        $this->router = $router;
    }

    public function getUrl(Storage $storage, $id) {
        return $this->router->generate(
            'storage_show_by_id',
            [
                'id' => $id,
            ]
        );
    }

    public function getDeleteUrl(Storage $storage, $id) {
        return $this->router->generate(
            'storage_delete_by_id',
            [
                'id' => $id,
            ]
        );
    }


    public function getUpdateUrl(Storage $storage, $id) {
        return $this->router->generate(
            'storage_modify_by_id',
            [
                'id' => $id,
            ]
        );
    }
}