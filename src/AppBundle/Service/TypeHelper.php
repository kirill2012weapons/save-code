<?php
namespace AppBundle\Service;

use AppBundle\Entity\Type;

class TypeHelper {

    private $router;

    public function __construct($router) {
        $this->router = $router;
    }

    public function getDeleteURL(Type $type) {
        return $this->router->generate(
            'types_delete_by_id',
            [
                'id' => $type->getID(),
            ]
        );
    }

    public function getMofidiedURL(Type $type) {
        return $this->router->generate(
            'types_modify_by_id',
            [
                'id' => $type->getID(),
            ]
        );
    }

}