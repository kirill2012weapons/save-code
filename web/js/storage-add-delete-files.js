$( document ).ready(function() {
    addDeleteMediaFiles();
});

function addDeleteMediaFiles() {

    var add, del = null;

    var numberOfInputs = 0;

    add = document.querySelector('#addFile');
    del = document.querySelector('#deleteFile');

    if (!add || !del) {
        console.dir('no bnts');
        return false;
    }

    add.addEventListener('click', function (event) {
        event.preventDefault();
        addInputiks();
    });

    del.addEventListener('click', function (event) {
        event.preventDefault();
        delInputiks();
    });

    function addInputiks() {
        var wrap = document.querySelector('[data-media]');

        if (!wrap) return false;

        var br = document.createElement('br');

        var vrapDiv = document.createElement('div');
        vrapDiv.classList.add('tr-title');
        vrapDiv.classList.add('padding-b-5');
        vrapDiv.setAttribute('data-media-inpt', '');
        vrapDiv.setAttribute('style', 'width: 400px;');

        var divError = document.createElement('div');
        divError.setAttribute('style', 'color: yellow;');
        vrapDiv.appendChild(divError);

        var input = document.createElement('input');
        input.classList.add('iz-input');
        input.classList.add('w-700');
        var count = +document.querySelector('[data-media]').getAttribute('data-media') + 1;
        input.setAttribute('name', 'storage_add_action[attachments][' + count + '][file]');
        document.querySelector('[data-media]').setAttribute('data-media', count);
        input.setAttribute('type', 'file');
        input.setAttribute('data-file', '');
        vrapDiv.appendChild(input);
        vrapDiv.appendChild(br);

        vrapDiv.appendChild(divError);

        var input = document.createElement('textarea');
        input.classList.add('iz-textarea');
        input.classList.add('w-990');
        input.classList.add('h-200');
        input.setAttribute('style', 'margin-top: 3px;');
        input.setAttribute('name', 'storage_add_action[attachments][' + count + '][attachment_description]');

        vrapDiv.appendChild(input);

        wrap.appendChild(vrapDiv);
        wrap.appendChild(br);
        wrap.appendChild(br);
    }

    function delInputiks() {
        var allInputs = document.querySelectorAll('[data-media-inpt]');
        if (!allInputs) return false;
        var count = +document.querySelector('[data-media]').getAttribute('data-media');
        if (count == 0) return false;
        allInputs[allInputs.length - 1].remove();
        document.querySelector('[data-media]').setAttribute('data-media', count - 1);
    }
}