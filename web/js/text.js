$(document).ready(initPage);
function initPage(){
    ImgTobg();
    mobileMenu();
    pageScrollTop();
    visualSlider();
    header_fixed_class();
    customSelect();
    modal();
    anchor();

    $(".content-scroll").mCustomScrollbar({
        axis:"y",
        theme:"light",
        scrollButtons: false,
    });
}

function ImgTobg() {
    $('.img-to-bg').each(function() {
        if ($(this).find('img').length) {
            $(this).css('background-image', 'url(' + $(this).find('> img').attr('src') + ')');
        }
    });
}

function mobileMenu(){
    $('<span class="open-menu"><span></span><span></span><span></span><span></span></span>').appendTo('.header-page > .container');
    $('<span class="fader"/>').appendTo('.header-page > .container');
    $('html').on('click', '.open-menu', function() {
        $('body').toggleClass('menu-opened');
        return false;
    });
    $('.fader').on('click touchmove', function(event) {
        $('body').removeClass('menu-opened');
    });
}

function pageScrollTop() {
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.btn-page-up').fadeIn();
        } else {
            $('.btn-page-up').fadeOut();
        }
    });
    $('.btn-page-up').click(function(e){
        var offsetTop = $('body').offset().top;
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 500);
        e.preventDefault();
    });
}

function visualSlider(){
    $('.slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        fade: true,
        speed: 2000,
        autoplaySpeed: 2000,
        autoplay: true,
    });
}

function header_fixed_class(){
    var header = $('.header-page');
    var wrapper = $('.wrapper');
    var heightEl  = 0;
    $(window).scrollTop() > heightEl ? header.addClass('modify') : header.removeClass('modify');
    $(window).scrollTop() > heightEl ? wrapper.addClass('header_fixed') : wrapper.removeClass('header_fixed');
    $(window).scroll(function(event){
        //event.stopImmediatePropagation();
        $(window).scrollTop() > heightEl ? header.addClass('modify') : header.removeClass('modify');
        $(window).scrollTop() > heightEl ? wrapper.addClass('header_fixed') : wrapper.removeClass('header_fixed');
    });
}

function customSelect(){
    jcf.setOptions('Select', {
        wrapNative: false,
        wrapNativeOnMobile: false,
        useCustomScroll: false,

    });

    jcf.replaceAll();
}

function modal() {
    $('.btn-open-modal').on('click', function(event) {
        var modal = $($(this).attr('href'));
        $('body').addClass('modal-open');
        $('.modal').removeClass('modal_show');
        modal.addClass('modal_show');
        event.preventDefault();
    });

    $('.modal').on('click', function(event) {
        if ($(event.target).is('.modal__dialog')) {
            event.stopPropagation();
            $(this).removeClass("modal_show");
            $("body").removeClass("modal-open");
            $('#modal-window01').scrollTop(0);
            $('#modal-window02').scrollTop(0);
            $('#modal-window03').scrollTop(0);
            $('#modal-window-contact').scrollTop(0);
            $('#modal-window-list').scrollTop(0);
            $('#modal-window-top').scrollTop(0);
        }
    });
    $('.modal__close').on('click', function(event) {
        $('body').removeClass('modal-open');
        $(this).closest('.modal').removeClass('modal_show');
        $('#modal-window01').scrollTop(0);
        $('#modal-window02').scrollTop(0);
        $('#modal-window03').scrollTop(0);
        $('#modal-window-contact').scrollTop(0);
        $('#modal-window-list').scrollTop(0);
        $('#modal-window-top').scrollTop(0);
        event.preventDefault();
    });
    $('.btn-close-success').on('click', function(event) {
        $('body').removeClass('modal-open');
        $(this).closest('.modal').removeClass('modal_show');
        $('#modal-window01').scrollTop(0);
        $('#modal-window02').scrollTop(0);
        $('#modal-window03').scrollTop(0);
        $('#modal-window-contact').scrollTop(0);
        $('#modal-window-list').scrollTop(0);
        $('#modal-window-top').scrollTop(0);
        event.preventDefault();
    });
}

(function() {
    if (!String.prototype.trim) {
        (function() {
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function() {
                return this.replace(rtrim, '');
            };
        })();
    }
    [].slice.call( document.querySelectorAll( '.form-control' ) ).forEach( function( inputEl ) {
        // in case the input is already filled..
        if( inputEl.value.trim() !== '' ) {
            classie.add( inputEl.parentNode, 'input--filled' );
        }
        // events:
        inputEl.addEventListener( 'focus', onInputFocus );
        inputEl.addEventListener( 'blur', onInputBlur );
    } );
    function onInputFocus( ev ) {
        classie.add( ev.target.parentNode, 'input--filled' );
    }
    function onInputBlur( ev ) {
        if( ev.target.value.trim() === '' ) {
            classie.remove( ev.target.parentNode, 'input--filled' );
        }
    }
})();

function anchor(){
    $(".anchor, .btn-page-down").click(function(e){
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top - $('.header-page').outerHeight();
        // for modal popup
        $(this).closest('.modal').removeClass('modal_show');
        $('body').removeClass('menu-opened modal-open');

        $("html, body").stop().animate({
            scrollTop: offsetTop
        }, 1000);
        e.preventDefault();
    });
};
