<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <meta name="description" content="手術の新しいトレーニングスタイルを提供。手術器機の操作習得を目的としたスキルトレーニングから、
術者・助手・スコピストの全員が手術を「疑似体験」できる
新しいトレーニングスタイルまで、幅広い用途に活用いただけます。">
    <meta name="keywords" content="">
    <title>FasoLab</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700" rel="stylesheet">
</head>
<?php wp_head(); ?>
<body>
<div class="wrapper" <?php if (is_user_logged_in()) echo 'style="padding-top: 38px;"' ?>>
    <div class="page">
        <header class="header-page">
            <h1 class="logo">
                <a href="<?= home_url(); ?>" class="logo__link"></a>
            </h1>
            <div class="main-nav">
                <ul class="main-nav__list">
                    <li class="main-nav__item">
                        <a href="<?= home_url('/feature/') ?>" class="main-nav__link"><span class="main-nav__eng">Feature</span>特長</a>
                    </li>
                    <li class="main-nav__item">
                        <a href="<?= get_post_type_archive_link('introductions_p_t'); ?>" class="main-nav__link"><span class="main-nav__eng">Product</span>製品紹介</a>
                    </li>
                    <li class="main-nav__item">
                        <a href="<?= home_url('/voice/') ?>" class="main-nav__link"><span class="main-nav__eng">Voice</span>お客様の声</a>
                    </li>
                    <li class="main-nav__item">
                        <a href="<?= get_post_type_archive_link('news_p_t'); ?>" class="main-nav__link"><span class="main-nav__eng">News</span>お知らせ</a>
                    </li>
                </ul>
                <a href="<?= home_url('/contact/') ?>" class="main-nav__btn btn">Contact</a>
            </div>
        </header>
        <?php if (is_page('front-page') ||
        is_page_template('archive-introductions_p_t.php') ||
        is_page_template('page-privacy_policy.php')) : ?>

        <div class="page-line">

        <?php endif; ?>
