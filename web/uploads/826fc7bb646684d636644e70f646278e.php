<?php

define('__ROOT_THEME__', dirname(__FILE__));

function dump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

function dump_e($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    exit(0);
}

/*
 * MAIN defines
 */
define('SERVER_HOST', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);

define('IMG_DIR', SERVER_HOST . '/wp-content/themes/html_fasolab/public/images/');
define('CSS_DIR', SERVER_HOST . '/wp-content/themes/html_fasolab/public/css/');
define('JS_DIR', SERVER_HOST . '/wp-content/themes/html_fasolab/public/js/');
define('PDF_DIR', SERVER_HOST . '/wp-content/themes/html_fasolab/public/pdf/');

define('JS_PHP_DIR', SERVER_HOST . '/wp-content/themes/php_fasolab/asserts/js/');
define('IMG_PHP_DIR', SERVER_HOST . '/wp-content/themes/php_fasolab/asserts/images/');
define('CSS_PHP_DIR', SERVER_HOST . '/wp-content/themes/php_fasolab/asserts/css/');
define('PDF_PHP_DIR', SERVER_HOST . '/wp-content/themes/php_fasolab/asserts/pdf/');

/*
 * REQs
 */

//Delete from the admin panel
require_once( __ROOT_THEME__ . '/helpers/remove-admin-smth.php');

//Req scripts and styles
require_once( __ROOT_THEME__ . '/helpers/register-scripts.php');

//Register image SIZES
require_once( __ROOT_THEME__ . '/helpers/register-image-size.php');

//ACF OPTIONS
require_once( __ROOT_THEME__ . '/helpers/register-acf-options.php');

//AJAX Validations and template send html MAIL
require_once( __ROOT_THEME__ . '/helpers/ajax-validation.php');

//if (WP_DEBUG) {
//    define( 'WP_HOME', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
//    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
//    $uploads = wp_upload_dir();
//}

require __ROOT_THEME__ . '/classes/B_Crumbs.php';
require __ROOT_THEME__ . '/classes/Pagination_show.php';

function htmlShowLoadTemplating($display = null, $height='auto') {

    if (!is_null($display)) {
        $loading = <<<HTML
    <div class="loader-custom custom-nb" style="display: none;">
        <div class="nb-spinner pos-centered nb-spinner"></div>
    </div>
HTML;
    } else {
        $loading = <<<HTML
    <div class="loader-custom custom-nb" style="height: {$height};">
        <div class="nb-spinner pos-centered nb-spinner"></div>
    </div>
HTML;
    }

    echo $loading;

}

function get_custom_single_template($single_template) {
    global $post;

    if ($post->post_type == 'introductions_p_t') {
        $terms = get_the_terms($post->ID, 'introductions_tax');
        if($terms && !is_wp_error( $terms )) {
            //Make a foreach because $terms is an array but it supposed to be only one term
            foreach($terms as $term){
                $single_template = dirname( __FILE__ ) . '/single-introductions_p_t-' . $term->slug.'.php';
            }
        }
    }
    return $single_template;
}

add_filter( "single_template", "get_custom_single_template" ) ;