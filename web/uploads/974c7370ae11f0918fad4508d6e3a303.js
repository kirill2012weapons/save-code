$(document).ready(initPage);
function initPage(){
	conversionImages();
	mobileMenu();
	anchor();
	scrollTop();

    if ($('#company_form_send').length) {
        contactForm();
    }

}

function conversionImages(){
	$('.bg').each(function() {
		$(this).css('background-image', 'url(' + $(this).find('> img').attr('src') + ')').find('> img').hide();
	});
}

function mobileMenu(){
	$('<a href="#" class="open-menu"><span></span>Open Menu</a>').prependTo('#header');
	$('.open-menu, .close').click(function(){
		$('body').toggleClass('menu-opened');
		return false;
	});
	$('.main-nav .btn.btn-default').click(function(){
		$('body').toggleClass('menu-opened');
		return false;
	});
	$('#header .nav li a').click(function(){
		$('body').removeClass('menu-opened');
	});
	
	$(document).on('touchmove', function(event) {
		if ($('body').hasClass('menu-opened') && $(window).width() < 992) {
			if ($('#main-nav').has(event.target).length) {
			return true;
			} else {
				event.preventDefault();
			}
		}
	});
}


function scrollTop(){
	$('#up-arrow').click(function(){
		$('html, body').animate({scrollTop : 0},500);
		return false;
	});
}

function anchor(){
	$(".anchor").click(function(e){
		var href = $(this).attr("href"),
		offsetTop = href === "#" ? 0 : $(href).offset().top - $('#header').outerHeight();
		$('body').removeClass('menu-opened');
		$("html, body").stop().animate({
			scrollTop: offsetTop
		}, 1000);
		e.preventDefault();
	});
};

var OnScroll_a = function() {

	$(document).ready(function(){
		setTimeout(OnScroll,300);
	});
	if(window.location.hash){
		window.hash = window.location.hash.substr(1);
		window.location.hash = '';
	}
	function OnScroll() {
		if (window.hash) {
			var scrollPos = $('#'+window.hash).offset().top -= document.querySelector('#header').offsetHeight;
			$("html, body").animate({ scrollTop: scrollPos }, 1000);
		}
	};

}();

function contactForm(){

    var url_  = '../common/php/contact_send.php';
    var fName = '#company_form_send';

    $(fName).validationEngine('attach', {
        ajaxFormValidation: true,
        onBeforeAjaxFormValidation: submitData
    });

    function submitData() {

        var f = $(fName);
        var method_ = f.prop('method');
        var formdata = new FormData(f.get(0));

        $.ajax({
            url    : url_,
            method : method_,
            type   : 'POST',
            data : formdata,
            cache       : false,
            contentType : false,
            processData : false,
            success: function(data) {
                $('html, body').animate({scrollTop: $('[data-form-mail]').offset().top - 10});
                $('#company-form').hide('slow');
                $('[data-form-mail]').append(
                    '<div class="holder"><p>' +
                    'この度は【便利屋元さん】へお問い合わせ頂き誠にありがとうございます。' +
                    '<br>内容を確認し、担当者よりご連絡致します。' +
                    '</p></div>'
                );
            }
        }).fail(function(data) {
            alert('送信失敗しました！');
        });
    }
}